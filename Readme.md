This example demonstrates the `tabsetPanel` and `tabPanel` widgets.

Notice that outputs that are not visible are not re-evaluated until they become visible. Try this: 

1. Scroll to the bottom of the `server` function. You might need to use the *show with app* option so you can easily view the code and interact with the app at the same time.
2. Change the number of observations, and observe that only `output$plot` is evaluated.
3. Click the Summary tab, and observe that `output$summary` is evaluated.
4. Change the number of observations again, and observe that now only `output$summary` is evaluated.

the example uses a custom function `my_hist` from a user package `myLib` located in the `src` subfolder

```
myLib::my_hist(d(), main = paste("r", dist, "(", n, ")", sep = ""))
```

after making changes to the function, e.g. change "green" to "red" in `./src/myLib/R/my_hist.R`

```
my_hist <- function(...) {
  hist(col = "#75AADB", border = "green", ...)
}
```

re-build package

```
devtools::build("./src/myLib")
```

re-install package to local library

```
install.packages("./src/myLib_0.0.0.9000.tar.gz", lib = "./lib", repos = NULL, type = "source")
```

run app

```
bash run_shiny.sh
```

alternatively, `devtools::load_all(./src/myLib)` can be used to avoid the need for building and installing the package
